<?php
// $Id$

/**
 * @file
 * Provides the settings form for the Metatag Robots module.
 */

  function metatag_robots_admin_settings_form(&$form_state) {

    $form['metatag_robots_enable']=array(
      '#type' => 'radios',
      '#title' => 'Enable robots metatag?',
      '#description' => t('This option also you to enable either the "index, follow" or "noindex, nofollow" metatag in the template'),
      '#default_value' => variable_get('metatag_robots_enable', array()),
      '#options' => array(t('index follow'), t('noindex nofollow')),
      '#weight' => -2,
    );
   
    return system_settings_form($form);

  } 
